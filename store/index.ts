import {defineStore} from "pinia";
import {TodoInterface} from "~/types/todoInterface";
import {RootState} from "~/types/RootState";
import axiosInstance from "~/services/_instance";


interface res{
    'data': TodoInterface[]
}

const url = "/api/todos";
export const useTodoStore = defineStore("todo", {
        state: () => {
            return {todos: [], error: false, showModal: false, loadingTodo: false} as RootState;
        },
        getters: {
            doneTodos: (state): number => {
                return state.todos.filter((item) => item.done == true).length
            }
        },
        actions: {
            getTodos() {
                axiosInstance.get<res>(url)
                    .then((res) => {
                        console.log(res.data)
                        this.loadingTodo = true;
                        this.todos = res.data.data;
                    });
            },
            addTodos(todo: TodoInterface): void {
                axiosInstance.post<TodoInterface>(url,
                    {
                        header: todo.header,
                        text: todo.text,
                        done: todo.done,
                        created_at: todo.created_at,
                        date_expired: todo.date_expired,
                    })
                    .then((res) => {
                        this.todos.push(res.data);
                    })
                    .catch((e) => {
                        this.error = true;
                    });
            },
            editTodo(todo: any) {
                axiosInstance.patch<TodoInterface>(`${url}/${todo._id}`,
                    {
                        ...todo
                    })
                    .then((res) => {
                        this.todos = this.todos.map((item) => {
                            if (item._id === todo._id) {
                                return res.data;
                            }
                            return item
                        })
                    });
            },
            toggleDone(todo: TodoInterface) {
                axiosInstance.patch<TodoInterface>(`${url}/${todo._id}`,
                    {
                        done: !todo.done,
                    }).then((res) => {
                    const el = this.todos.filter((item) => item._id == todo._id)[0];
                    el.done = !el.done;
                });
            },
            deleteTodos<TodoInterface>(_id: string): void {
                axiosInstance.delete(`${url}/${_id}`,
                ).then((res) => {
                    this.todos = this.todos.filter((item) => item._id !== _id);
                });
            },
            setDone(_id: string): void {
                let todo = this.todos.find((item) => item._id === _id);
                if (todo !== undefined) {
                    todo.done = !todo.done;
                }
            },
        },
    })
;
