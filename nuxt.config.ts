// https://nuxt.com/docs/api/configuration/nuxt-config

// import {useRuntimeConfig} from 'nuxt3';

// const BASE_URL = useRuntimeConfig().public.api_url


export default defineNuxtConfig({
    css: ["@/assets/styles/main.sass", '@morev/vue-transitions/styles'],
    modules: ['@pinia/nuxt'],
    ssr: true,
    app: {
        head: {
            link: [{rel: 'icon', type: 'image/png', href: '/favicon.png'}]
        },
    },
    nitro: {
        devProxy: {
            "/api": {
                target: process.env.BASE_URL+ '/api/',
                changeOrigin: true,
                prependPath: true,
            }
        }
    },
})
;